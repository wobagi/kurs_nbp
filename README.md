# kurs_nbp

Simple CLI tool for downloading NBP exchange rates.

Usage:
```
NBP currency rates

optional arguments:
  -h, --help            show this help message and exit
  -d DATE, --date DATE  Date in ISO format, YYYY-MM-DD
  -c CURRENCY, --currency CURRENCY
                        Currency ISO code, i.e USD, EUR
```