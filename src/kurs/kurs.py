#!/usr/bin/env python3

import sys
import argparse
import urllib.request
import xml.etree.ElementTree as xml_tree
import datetime as dt

from typing import List
from dataclasses import dataclass

XML_TABLE = "numer_tabeli"
XML_DATE  = "data_publikacji"
XML_ROW   = "pozycja"
XML_CODE  = "kod_waluty"
XML_RATE  = "kurs_sredni"

NAMES_URL = "http://www.nbp.pl/kursy/xml/dir{year}.txt"
TABLE_URL = "http://www.nbp.pl/kursy/xml/{filename}.xml"


@dataclass
class Retrieval():
    date: str
    table: str
    currency: str
    rate: float


def iso_date(d: str) -> dt.date:
    """
    Type validator for argument parser
    """
    return dt.datetime.strptime(d, "%Y-%m-%d")


def url_for_date(date: dt.date) -> str:
    """
    Return url with table list for the year corresponding to given date
    """
    year_suffix = str(date.year) if date.year < dt.datetime.now().year else ""
    return NAMES_URL.format(year=year_suffix)


def filenames_at_url(url: str) -> List[str]:
    """
    Return all filenames listed under given url
    """
    with urllib.request.urlopen(url) as dir:
        return [f.strip().decode('UTF-8') for f in dir.readlines()]


def get_all_tables(date: dt.date) -> List[str]:
    # get list of tables for two consecutive years
    # to prevent invalid retrievals at the turn of the year
    last_year = filenames_at_url(url_for_date(
        dt.date(date.year - 1, date.month, date.day))
    )
    this_year = filenames_at_url(url_for_date(date))
    return [s for s in last_year + this_year if s.startswith("a")]


def recent_valid_table(date: dt.date) -> str:
    table_list = get_all_tables(date)

    # Get just the tables up to given date.
    # Last 6 chars of table name contain timestamp.
    tables_up_to_date = [s for s in table_list if s[-6:] <= f"{date:%y%m%d}"]

    # Return the most recent table for given date. Max on the last 6 chars.
    return max(tables_up_to_date, key=lambda x: x[-6:])


def get_rate(date: dt.date, currency: str) -> Retrieval:
    filename = recent_valid_table(date)
    if filename:
        url = TABLE_URL.format(filename=filename)
        f = urllib.request.urlopen(url)
        root = xml_tree.fromstring(f.read())
        rec_table = root.find(XML_TABLE).text
        rec_date = root.find(XML_DATE).text
        for c in root.findall(XML_ROW):
            rec_code = c.find(XML_CODE).text
            if rec_code == currency:
                rec_rate = c.find(XML_RATE).text.replace(",", ".") or 0
                break
        if not rec_rate:
            sys.exit("No data found for this currency code.")
    return Retrieval(
        date=rec_date,
        table=rec_table,
        currency=rec_code,
        rate=float(rec_rate)
    )

def run(args):
    r = get_rate(args.date, args.currency)    
    print(f"{r.currency}: {r.rate:.4f} wg tabeli NBP nr {r.table} z dnia {r.date}")

def main():
    parser = argparse.ArgumentParser(description="NBP currency rates")
    parser.add_argument("-d", "--date", type=iso_date, default=dt.datetime.now(), help="Date in ISO format, YYYY-MM-DD")
    parser.add_argument("-c", "--currency", default="USD", help="Currency ISO code, i.e USD, EUR")
    args = parser.parse_args()
    
    run(args)

if __name__=="__main__":
    main()